<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    use HasFactory;
    protected $fillable = ['user_student_id', 'return_date', 'user_entry_id', 'status'];


    public function borrow_details()
    {
        return $this->hasMany(BorrowDetail::class);
    }
}
