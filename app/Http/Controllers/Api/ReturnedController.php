<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ReturnBook;
use App\Models\Borrow;
use App\Models\BorrowDetail;
use Illuminate\Support\Facades\DB;
use App\Models\Book;

class ReturnedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            DB::beginTransaction();
            // validate request
            $request->validate([
                'user_student_id' => 'required|exists:users,id',
                'books' => 'required',
                'borrow_id' => 'required|exists:borrow,id',
            ]);

            $user = auth()->user();
            Borrow::where('id', $request->borrow_id)->update([
                'status' => 2,
            ]);
            foreach ($request->books as $book) {
                ReturnBook::create([
                    'borrow_id' => $request->borrow_id,
                    'book_id' => $book,
                    'student_user_id' => $request->user_student_id,
                    'user_entry_id' => $user->id,
                    'status' => 1,
                ]);
                Book::where('id', $book)->update([
                    'copies' => DB::raw('copies + 1'),
                ]);
                BorrowDetail::where('book_id', $book)
                    ->where('borrow_id', $request->borrow_id)
                    ->update([
                        'status' => 2,
                    ]);
            }
            DB::commit();
            $data = [
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            DB::rollBack();
            $data = [
                'status' => 500,
                'message' => 'error',
                'data' => $e->getMessage()
            ];
            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
