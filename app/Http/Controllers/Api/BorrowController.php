<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Borrow;
use App\Models\BorrowDetail;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Models\Book;

class BorrowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            // validate request
            DB::beginTransaction();


            $request->validate([
                'user_student_id' => 'required|exists:users,id',
                'books' => 'required',
            ]);

            $return_date = Carbon::now()->addDays(30);
            $borrow_header = Borrow::create([
                'user_student_id' => $request->user_student_id,
                'return_date' => $return_date,
                'status' => 1,
            ]);
            foreach ($request->books as $book) {
                BorrowDetail::create([
                    'borrow_id' => $borrow_header->id,
                    'book_id' => $book,
                    'student_user_id' => $request->user_student_id,
                    'user_entry_id' => $request->user_entry_id,
                    'status' => 1,
                ]);
                Book::where('id', $book)->update([
                    'copies' => DB::raw('copies - 1'),
                ]);
            }
            DB::commit();
            $data = [
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            DB::rollBack();
            $data = [
                'status' => 500,
                'message' => 'error',
                'data' => $e->getMessage()
            ];
            return response()->json($data);
        }
    }
    public function extend(Request $request, $id)
    {
        try {
            $borrow = Borrow::find($id);
            $borrow->return_date = Carbon::now()->addDays(30);
            $borrow->save();
            $data = [
                'status' => 200,
                'message' => 'success',
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            $data = [
                'status' => 500,
                'message' => 'error',
                'data' => $e->getMessage()
            ];
            return response()->json($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
}
