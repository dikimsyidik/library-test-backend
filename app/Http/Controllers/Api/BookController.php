<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        try {
            $books = Book::all();
            $data = [
                'status' => 200,
                'message' => 'success',
                'data' => $books
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            $data = [
                'status' => 500,
                'message' => 'error',
                'data' => $e->getMessage()
            ];
            return response()->json($data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $book = Book::find($id);
            $data = [
                'status' => 200,
                'message' => 'success',
                'data' => $book
            ];
            return response()->json($data);
        } catch (\Exception $e) {
            $data = [
                'status' => 500,
                'message' => 'error',
                'data' => $e->getMessage()
            ];
            return response()->json($data);
        }
    }
}
