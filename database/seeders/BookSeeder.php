<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Book;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //truncate Book 
        Book::truncate();
        //create 10 books
        $books = [
            ['title' => 'The Alchemist', 'author' => 'Paulo Coelho', 'copies' => 10],
            ['title' => 'The Kite Runner', 'author' => 'Khaled Hosseini', 'copies' => 10],
            ['title' => 'The Da Vinci Code', 'author' => 'Dan Brown', 'copies' => 10],
            ['title' => 'The Godfather', 'author' => 'Mario Puzo', 'copies' => 10],
            ['title' => 'The Notebook', 'author' => 'Nicholas Sparks', 'copies' => 10],
            ['title' => 'The Alchemist', 'author' => 'Paulo Coelho', 'copies' => 10],
            ['title' => 'The Kite Runner', 'author' => 'Khaled Hosseini', 'copies' => 10],

        ];
        foreach ($books as $book) {
            Book::create($book);
        }
    }
}
